Url Shortener API Package for Laravel 5.5.

Installation

To install, run the following in your project directory:

composer require divart/shortlink

Then in config/app.php add the following to the providers array:

\DivArt\ShortLink\ShortLinkServiceProvider::class,

Also in config/app.php, add the Facade class to the aliases array:

'Shorty'    => Mbarwick83\Shorty\Facades\Shorty::class


Configuration

To publish Shorty's configuration file, run the following vendor:publish command:

php artisan vendor:publish --provider="DivArt\ShortLink\ShortLinkServiceProvider"

This will create a shortlink.php in your config directory. Here you must enter your Shortener URL API Key.


Usage

Be sure to include the namespace for the class wherever you plan to use this library:

use DivArt\ShortLink\Facades\ShortLink;

To shorten a URL:

$url = "https://www.google.com";

ShortLink::google($url);

// return https://goo.gl/Njku

To get stats of clicks on shortened URL:

$url = https://goo.gl/Njku;

ShortLink::googleStatistics($url);


Methods

ShortLink::google();

ShortLink::googleStatistics();

ShortLink::bitly();


License

The MIT License (MIT).